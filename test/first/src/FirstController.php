<?php

namespace test\first;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use test\first\Model\Add;

class FirstController extends Controller
{
    public function add(){
        return view('test::add');
    }
    public function addResult(Request $request){
        $result =  $request->first + $request->second;
        $add = new Add();
        $add->first = $request->first;
        $add->second = $request->second;
        $add->result = $result;
        $add->save();
        return view('test::result',compact('result'));
    }
}
