<?php

namespace test\first;

use Illuminate\Support\ServiceProvider;

class FirstServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('test\first\FirstController');
        $this->loadViewsFrom(__DIR__.'/views','test');
        $this->loadMigrationsFrom (__DIR__.'/database/migrations','test');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
    }
}
